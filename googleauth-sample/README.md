# How to use google sign-in

1. Get Client ID from [Developers Console](https://console.developers.google.com)
(Permit `http://localhost:3000`)

2. Put your Client ID into `index.html`

3. `lein ring server`
