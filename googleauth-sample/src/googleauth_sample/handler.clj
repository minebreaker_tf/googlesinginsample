(ns googleauth-sample.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [ring.util.response :as resp]
            [cheshire.core :refer :all])
  (:import (java.net InetSocketAddress
                     Proxy
                     Proxy$Type)
           (com.google.api.client.googleapis.auth.oauth2 GoogleIdToken
                                                         GoogleIdTokenVerifier)
           (com.google.api.client.googleapis.util Utils)
           (com.google.api.client.http.javanet NetHttpTransport
                                               NetHttpTransport$Builder)))

(def httpTransport
  (-> (new NetHttpTransport$Builder)
    (. setProxy
      (Proxy/NO_PROXY))
    (. build)))

(defn get-info [token]
  (do
    (-> (new GoogleIdTokenVerifier httpTransport (Utils/getDefaultJsonFactory))
      (. verify token)
      (. getPayload)
      (generate-string)
      (println))
    "<h2>Authentication info received.</h2>"))

(defroutes app-routes
  (GET "/" [] (resp/redirect "/index.html"))
  (POST "/auth" [idtoken] (get-info idtoken))
  (route/not-found "Not Found"))

(def app
  (wrap-defaults app-routes (assoc-in site-defaults [:security :anti-forgery] false))) ; Disable anti-CSRF token (for testing)
